package com.epam.task3.logic;

import com.epam.task3.entity.GemStone;

import java.util.*;

/**
 * @author Ihor Kovalyk
 * @version 1.0
 * @since 2019-08-06
 */

public class Necklace {

    private List<GemStone> gemStones = new ArrayList<>();

    public void gemStonesDisplay() {
        for (GemStone stone : gemStones) {
            System.out.println(stone);
        }
    }

    public GemStone getByName(String name) {
        GemStone result = gemStones.stream()
                .filter(x -> name.equals(x.getName()))
                .findAny()
                .orElse(null);
        return result;

    }

    public void sortStonesByPrice() {
        Collections.sort(this.gemStones);
    }

    public ArrayList<GemStone> filterByPrice(int min, int max) {
        ArrayList<GemStone> sortingPrice = new ArrayList<>();
        for (int i = 0; i < gemStones.size(); i++) {
            double price = gemStones.get(i).getPrice();
            if (price >= min && price <= max) {
                sortingPrice.add(gemStones.get(i));
            }
        }
        return sortingPrice;
    }

    public void showStones(List<GemStone> stones) {
        System.out.println();
        for (int i = 0; i < stones.size(); i++) {
            System.out.println(stones.get(i));
        }
    }

    public List<GemStone> getGemStones() {
        return gemStones;
    }

    public void setGemStones(List<GemStone> gemStones) {
        this.gemStones = gemStones;
    }

}
