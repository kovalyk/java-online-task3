package com.epam.task3.exceptions;

public class EmailNotUniqueException extends Exception {

    EmailNotUniqueException(String message) {
        super(message);
    }
}
