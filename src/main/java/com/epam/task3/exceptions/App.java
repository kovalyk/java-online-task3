package com.epam.task3.exceptions;

public class App {

    public static void main(String[] args) {
        RegistrationService service = new RegistrationService();
        try {
            service.validateEmail("one@gmail.com");
        } catch (EmailNotUniqueException e) {
            System.err.print(e);
        }
    }
}
