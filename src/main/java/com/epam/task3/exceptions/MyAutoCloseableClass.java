package com.epam.task3.exceptions;

public class MyAutoCloseableClass implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("Closed MyAutoCloseableClass");
    }
}
