package com.epam.task3.exceptions;

import java.util.Arrays;
import java.util.List;

public class RegistrationService {

    List<String> registeredEmails = Arrays.asList("one@gmail.com", "two@gmail.com", "three@gmail.com");

    public void validateEmail(String email) throws EmailNotUniqueException {

        if (registeredEmails.contains(email)) {
            throw new EmailNotUniqueException("Email Already Registered");
        }
    }
}
