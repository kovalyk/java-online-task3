package com.epam.task3.entity;

import com.epam.task3.enums.GemType;
import com.epam.task3.enums.Transparency;

import java.util.Objects;

public class GemStone implements Comparable<GemStone> {
    private static final String PATTERN = "%-15s %-15s %-15s %-15s";

    private String name;
    private double weight;
    private double price;
    private Transparency transparency;
    private GemType gemType;


    public GemStone(String name, double price, double weight, Transparency transparency, GemType gemType) {
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.transparency = transparency;
        this.gemType = gemType;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Transparency getTransparency() {
        return transparency;
    }

    public void setTransparency(Transparency transparency) {
        this.transparency = transparency;
    }

    public GemType getGemType() {
        return gemType;
    }

    public void setGemType(GemType gemType) {
        this.gemType = gemType;
    }

    @Override
    public String toString() {

             return "GemStone{" +
                "name='" + name + '\'' +
                ", weight=" + weight + "ct" +
                ", price= $" + price +
                ", transparency=" + transparency +
                ", gemType=" + gemType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GemStone gemStone = (GemStone) o;
        return Double.compare(gemStone.price, price) == 0 &&
                name.equals(gemStone.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

    @Override
    public int compareTo(GemStone stone) {
        int result;
        if (this.price > stone.price) {
            result = 1;
        } else if (this.price < stone.price) {
            result = -1;
        } else {
            result = 0;
        }
        return result;
    }
}
