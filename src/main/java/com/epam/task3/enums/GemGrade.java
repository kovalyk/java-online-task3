package com.epam.task3.enums;

public enum GemGrade {
    PRECIOUS, SEMIPRECIOUS
}
