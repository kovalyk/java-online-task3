package com.epam.task3.enums;

public enum GemType {
    DIAMOND(GemGrade.PRECIOUS),
    RUBY(GemGrade.PRECIOUS),
    EMERALD(GemGrade.PRECIOUS),
    SAPPHIRE(GemGrade.PRECIOUS),
    SPINEL(GemGrade.SEMIPRECIOUS),
    QUARTZ(GemGrade.SEMIPRECIOUS),
    ONYX(GemGrade.SEMIPRECIOUS),
    CITRINE(GemGrade.SEMIPRECIOUS),
    AMETHYST(GemGrade.SEMIPRECIOUS);

    private GemGrade gemGrade;

    GemType(GemGrade precious) {
        this.gemGrade = precious;
    }

    public GemGrade getGemGrade() {
        return gemGrade;
    }

    public void setGemGrade(GemGrade gemGrade) {
        this.gemGrade = gemGrade;
    }
}
