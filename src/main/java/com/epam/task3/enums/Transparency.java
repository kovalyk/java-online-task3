package com.epam.task3.enums;

public enum Transparency {
    OPAQUE, TRANSLUCENT, TRANSPARENT
}
