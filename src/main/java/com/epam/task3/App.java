package com.epam.task3;

import com.epam.task3.logic.Menu;

/**
 * Камни. Определить иерархию драгоценных и
 * полудрагоценных камней. Отобрать камни для
 * ожерелья. Подсчитать общий вес (вкаратах) и
 * стоимость. Провести сортировку камней ожерелья
 * на основе ценности. Найти камни в ожерелье,
 * соответствующие заданному диапазону параметров
 * прозрачности.
 *
 *   @author Ihor Kovalyk
 *   @version 1.0
 *   @since 2019-08-06
 *
 */

public class App {

    public static void main(String[] args) {

        Menu menu = new Menu();
        menu.run();
    }
}

